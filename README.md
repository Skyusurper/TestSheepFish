# creave venv

python -m venv venv

# activate venv from SheepFish dir

source venv/bin/activate

# install requirements

pip install -r requirements.txt

# run docker-compose

docker compose up

# run celery

celery -A core.celery worker -l info

# run emulated printer

python asyncio_printer.py

# create use
python manage.py createsuperuser

# migrate
python manage.py migrate

# fixtures
python manage.py loaddata printers

# run server
python manage.py runserver
