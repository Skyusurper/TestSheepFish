import asyncio
import aiohttp
import random


API_KEY = "deb0e36e-2e04-48b8-9fff-5d8b06a3e853"
API_URL_CHECKS = f"http://127.0.0.1:8000/api/checks/{API_KEY}/"
API_URL_UPDATE = "http://127.0.0.1:8000/api/check_update/{check_id}/"


async def download_and_print_pdf(check_id):
    print(f"Downloading PDF for Check {check_id}...")
    await asyncio.sleep(random.uniform(1, 5))
    print(f"PDF for Check {check_id} downloaded successfully.")


async def poll_api_and_print():
    while True:
        # Poll the API every 30 seconds
        await asyncio.sleep(5)

        # Simulate API request and response
        async with aiohttp.ClientSession() as session:
            try:
                response = await session.get(API_URL_CHECKS)
                if response.status == 200:
                    checks = await response.json()
                    for check in checks:
                        check_id = check['id']
                        check_status = check['status']
                        print(check_id, check_status)

                        if check_status == 'rendered':
                            await download_and_print_pdf(check_id)

                            update_url = API_URL_UPDATE.format(
                                check_id=check_id
                            )

                            async with session.put(update_url) as u_response:
                                if u_response.status == 200:
                                    print(
                                        f"Check {check_id} status updated to 'printed'."
                                    )
                                else:
                                    print(
                                        f"Failed to update Check {check_id} status."
                                    )
                else:
                    print(
                        f"API request failed with status code {response.status}"
                    )
            except Exception as e:
                print(f"Error while making API request: {e}")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(poll_api_and_print())
