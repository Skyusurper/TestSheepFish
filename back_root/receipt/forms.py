from django import forms
from .models import Printer


class OrderForm(forms.Form):
    order_id = forms.IntegerField(
        label='Order ID',
        widget=forms.TextInput(attrs={'type': 'number'}),
        initial=1001,
    )
    shop = forms.ModelChoiceField(
        queryset=Printer.objects.values_list('point_id', flat=True).distinct(),
        label='Select the Shop',
        to_field_name="point_id"
    )
