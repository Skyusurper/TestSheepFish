import uuid
from django.db import models


class Printer(models.Model):
    name = models.CharField(max_length=100)
    api_key = models.CharField(
        max_length=36, unique=True, default=uuid.uuid4, editable=False
    )
    CHECK_TYPES = [
        ('kitchen', 'Kitchen'),
        ('client', 'Client'),
    ]
    check_type = models.CharField(max_length=10, choices=CHECK_TYPES)
    point_id = models.IntegerField()

    def __str__(self):
        return self.name


class Check(models.Model):
    CHECK_TYPES = [
        ('kitchen', 'Kitchen'),
        ('client', 'Client'),
    ]
    STATUS_CHOICES = [
        ('new', 'New'),
        ('rendered', 'Rendered'),
        ('printed', 'Printed'),
    ]

    printer = models.ForeignKey(Printer, on_delete=models.CASCADE)
    type = models.CharField(max_length=10, choices=CHECK_TYPES)
    order = models.JSONField()
    status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, default='new'
    )
    pdf_file = models.FileField(upload_to='pdfs/')

    def __str__(self):
        return f"{self.type} check for {self.printer.name}"
