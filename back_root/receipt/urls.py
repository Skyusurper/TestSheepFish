from django.urls import path
from .views import CreateChecksView

urlpatterns = [
    path('', CreateChecksView.as_view(), name='create-checks'),
]
