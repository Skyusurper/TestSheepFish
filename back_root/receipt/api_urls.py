from django.urls import path
from .views import CheckListAPIView

urlpatterns = [
    path(
        'api/checks/<str:key_api>/',
        CheckListAPIView.as_view(),
        name='check-list',
    ),
    path(
        'api/check_update/<int:check_id>/',
        CheckListAPIView.as_view(),
        name='check-detail',
    ),
]
