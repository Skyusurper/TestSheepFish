# Create your views here.
from celery import shared_task
import celery.signals as signals
from django.http import HttpResponse
from django.urls import reverse
from django.shortcuts import get_object_or_404, render, redirect
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView

from .forms import OrderForm
from .models import Check, Printer
from .serializers import CheckSerializer
from .tasks import generate_pdf_for_check


class CheckListAPIView(APIView):
    def get(self, request, key_api):
        checks = Check.objects.filter(printer__api_key=key_api)
        serializer = CheckSerializer(checks, many=True)
        return Response(serializer.data)

    def put(self, request, check_id):
        check = get_object_or_404(Check, id=check_id)
        check.status = 'printed'
        check.save()
        serializer = CheckSerializer(check)
        return Response(serializer.data)


class CreateChecksView(View):
    template_name = 'create_checks.html'

    def get(self, request):
        form = OrderForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = OrderForm(request.POST)
        if form.is_valid():
            order_id = form.cleaned_data['order_id']
            order_data = {'id_order': order_id}
            shop_id = form.cleaned_data['shop']

            printers = Printer.objects.filter(point_id=shop_id)

            if not printers.exists():
                return render(
                    request,
                    self.template_name,
                    {
                        'form': form,
                        'error_message': 'No printers available at this point',
                    },
                )

            checks_exists = Check.objects.filter(
                order__contains={'id_order': order_id}
            ).exists()

            if checks_exists:
                return render(
                    request,
                    self.template_name,
                    {
                        'form': form,
                        'error_message': f'Checks have already been generated for order {order_id}',
                    },
                )
            else:
                for print_item in printers:
                    for value, type in Check.CHECK_TYPES:
                        Check.objects.create(
                            printer=print_item,
                            type=value,
                            status='new',
                            order=order_data,
                            pdf_file=None,
                        )

                # run celery task for each check to generate pdf
                checks_new = Check.objects.filter(status='new')
                for check in checks_new:
                    generate_pdf_for_check.delay(check.id)

            return HttpResponse("Checks PDF generation has started.")

        else:
            return render(request, self.template_name, {'form': form})
