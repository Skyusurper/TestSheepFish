from django.contrib import admin

# Register your models here.
from .models import Printer, Check


class PrinterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'api_key', 'check_type', 'point_id')
    readonly_fields=('api_key',)

admin.site.register(Printer, PrinterAdmin)


class CheckAdmin(admin.ModelAdmin):
    list_display = ('id', 'printer', 'type', 'status', 'pdf_file')
    list_filter = ('printer', 'type', 'status')

admin.site.register(Check, CheckAdmin)
