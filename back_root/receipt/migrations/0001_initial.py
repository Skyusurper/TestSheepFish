# Generated by Django 4.2.4 on 2023-08-31 20:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Printer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('api_key', models.CharField(max_length=255)),
                ('check_type', models.CharField(choices=[('kitchen', 'Kitchen'), ('client', 'Client')], max_length=10)),
                ('point_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Check',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_check', models.CharField(choices=[('kitchen', 'Kitchen'), ('client', 'Client')], max_length=10)),
                ('order', models.JSONField()),
                ('status', models.CharField(choices=[('new', 'New'), ('rendered', 'Rendered'), ('printed', 'Printed')], default='new', max_length=10)),
                ('pdf_file', models.FileField(upload_to='pdfs/')),
                ('printer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='receipt.printer')),
            ],
        ),
    ]
