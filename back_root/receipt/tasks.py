import os
import subprocess
import tempfile

from celery import shared_task
from core.settings import MEDIA_ROOT, WKHTMLTOPDF_HOST, WKHTMLTOPDF_PORT
from django.template.loader import render_to_string

from .models import Check


@shared_task
def generate_pdf_for_check(check_id):
    check = Check.objects.get(pk=check_id)
    order_id = check.order['id_order']
    pdf_filename = f'{order_id}_{check.type}.pdf'
    pdf_file_path = os.path.join(MEDIA_ROOT, pdf_filename)

    context = {'check': check, 'order_id': order_id}
    html_content = render_to_string('receipt_template.html', context)

    temp_file_path = ''

    # tmp HTML
    with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as temp_file:
        temp_file.write(html_content.encode('utf-8'))
        temp_file_path = temp_file.name

    curl_command = [
        'curl',
        '-X',
        'POST',
        '-vv',
        '-F',
        f'file=@{temp_file_path}',
        f'http://{WKHTMLTOPDF_HOST}:{WKHTMLTOPDF_PORT}/',
        '-o',
        pdf_file_path,
    ]

    try:
        subprocess.run(curl_command, check=True)
        print('PDF generated successfully.')
    except subprocess.CalledProcessError as e:
        print(f'Error running curl: {e}')

    check.pdf_file = f'pdfs/{pdf_filename}'
    check.status = 'rendered'
    check.save()
